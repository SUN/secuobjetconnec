# -*- coding: utf-8 -*-
import base64

from Crypto.Cipher import AES

def Xor(string,key):
    """ Chiffrement Ou exclusif. string et key sont des chaines de caractÃ¨res."""

    tab1=' '.join([bin(ord(c)).replace('0b', '') for c in key])
  
    x = tab1.split(' ')
 
    tab1 = int(''.join([x for x in tab1 if x != " "]),2)
    tab = []
    for i in range (len(string)):
        tab.append(int(''.join(('0b', str(format(ord(string[i]), '08b')))), 2))
    output = ""

    for i in range(0, len(tab)):
        output += str(chr(tab[i] ^ tab1))
    print(output)
    return output

def Indice(table,element):
    """ Retourne l'indice d'Ã©lÃ©ment dans table"""
    indice=-1
    for i in range (0, len(table)):
        if table[i]==element:
            indice=i
    if indice==-1:
        print(erreur)
    return indice

def EncodeBase64(chaine):
    """ Encode en base 64 le paramÃ¨tre chaine"""
    encoded = base64.b64encode(chaine)
    return encoded

def DecodeBase64(chaine):
    """ DÃ©code la chaine encodÃ©e en base 64"""
    dec = base64.b64decode(chaine)
    return dec

def EncodeAES(chaine,key):
    """ Chiffrement AES 128 bits de chaine avec key comme clef.
        La taille de chaine est quelconque et sera complÃ©tÃ©e par le
        caractÃ¨re espace si nÃ©cessaire. Key est un tableau 16 Ã©lÃ©ments."""
    newkey=""
    i=0
    while i!=len(key):
        newkey=newkey+str(key[i])
        i=i+1
        
    # 初始化加密器
    aes = AES.new(add_to_16(newkey), AES.MODE_ECB)
    #先进行aes加密
    encrypt_aes = aes.encrypt(add_to_16(chaine))
    #用base64转成字符串形式
    encrypted_text = str(base64.encodebytes(encrypt_aes), encoding='utf-8')  # 执行加密并转码返回bytes
    print(encrypted_text)
        
    

def DecodeAES(chaine,key):
    """ DÃ©chiffre chaine. La clef key est un tableau de 16 Ã©lÃ©ments.
        Les caractÃ¨res espace en fin de la chaine rÃ©sultatante seront supprimÃ©s."""
    newkey=""
    i=0
    while i!=len(key):
        newkey=newkey+str(key[i])
        i=i+1
    # 初始化加密器
    aes = AES.new(add_to_16(newkey), AES.MODE_ECB)
    #优先逆向解密base64成bytes
    base64_decrypted = base64.decodebytes(chaine.encode(encoding='utf-8'))
    #执行解密密并转码返回str
    decrypted_text = str(aes.decrypt(base64_decrypted),encoding='utf-8').replace('\0','') 
    print(decrypted_text)

def Contient(aiguille,chaine):
    """ RÃ©sultat True si le paramÃ¨tre chaine contient la chaine aiguille."""
    if aiguille in chaine:
        bool=True
    else:
        bool=False
    return bool

def EstImprimable(caractere):
    """ Liste des caractÃ¨res imprimables :
    0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ """
    bool=False

 
    if ord(caractere)>=32 and ord(caractere)<=126:
        bool = True
    return bool
        
    if 32<=caractere<=126:
        return True
    else:
        return False

def Remplace(chaine,avant,apres):
    """ Remplace les occurrences de avant par apres dans chaine."""
    #i=0
    #n=len(avant)
    #while chaine[i]!=avant[i]:
    indice=chaine.find(avant)
    fin=indice+len(avant)
    if indice!=-1:
        chaine=chaine[:indice]+apres+chaine[fin:]
    return chaine

def Extraire(chaine,separation,n):
    """ Retourne la valeur du niÃ¨me champ de chaine.
        Les champs sont sÃ©parÃ©s par le caractÃ¨re sÃ©paration."""
    compteur=0
    tab=""
    j=0
    i=0
    while i!=len(chaine) and compteur!=n+1:
        if chaine[i]==separation:
            compteur+=1
            i=i+1
        if compteur==n:
            tab=tab+chaine[i]
        
        i=i+1
    n=int(tab)
    return n
            
def Format(n):
    """ Retourne une chaine de caractÃ¨res de 4 caractÃ¨res pour tout nombre entier de 0 Ã   9999
        Les valeurs seront prÃ©cÃ©dÃ©es de 0."""
    i=0
    n=str(n)
   
    tab=""
    diff=4-len(n)
    while diff!=0:
        tab=tab+"0"
        diff=diff-1
    
    return tab+n
       

def main():
    """ Tests, toutes les lignes sont correctes (rÃ©sultat : True). ComplÃ¨ter les fonctions."""
    #print (Xor(" $--.","A")=="Hello")
    #print (Xor("-/+-43","ABA")=="Bonjour")
    #ok print (Indice([1,2,3,4,5,6],3)==2)
    #ok print (EncodeBase64(b"Une Chaine")==b"VW5lIENoYWluZQ==")
    #ok print (DecodeBase64(b"VW5lIENoYWluZQ==")==b"Une Chaine")
    print (EncodeAES(b"Elements",[2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,6])==b"\xcb[q(\x07\xe70\xff\x13D\xfe\xf9\x9eQ\x08!")
    #print (DecodeAES(b"\xcb[q(\x07\xe70\xff\x13D\xfe\xf9\x9eQ\x08!",[2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,6])==b"Elements")
    #ok print (Contient("OK","Le resultat est OK !")==True)
    #ok print (Contient("OK","Le resultat est Ok !")==False)
    #ok print (EstImprimable("A")==True)
    #ok print (EstImprimable("\x07")==False)
    #ok print (EstImprimable(" ")==True)
    #ok print (Remplace("Ceci est une string typique","string","chaine")=="Ceci est une chaine typique")
    #ok print (Extraire("ROUGE,0034,4EF563",",",1)==34)
    #ok print (Format(3)=="0003")
    #ok print (Format(123)=="0123")
    return


if __name__ == '__main__':
    main()
