# -*- coding: utf-8 -*-

def Xor(string,key):
    """ Chiffrement Ou exclusif. string et key sont des chaines de caractères."""

def Indice(table,element):
    """ Retourne l'indice d'élément dans table"""

def EncodeBase64(chaine):
    """ Encode en base 64 le paramètre chaine"""

def DecodeBase64(chaine):
    """ Decode la chaine encodée en base 64"""

def EncodeAES(chaine,key):
    """ Chiffrement AES 128 bits de chaine avec key comme clef.
        La taille de chaine est quelconque et sera complétée par le
        caractÃ¨re espace si nécessaire. Key est un tableau 16 éléements."""

def DecodeAES(chaine,key):
    """ Dechiffre chaine. La clef key est un tableau de 16 éléments.
        Les caractères espace en fin de la chaine résultatante seront supprimés."""

def Contient(aiguille,chaine):
    """ Resultat True si le paramètre chaine contient la chaine aiguille."""

def EstImprimable(caractere):
    """ Liste des caractères imprimables :
        0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ """

def Remplace(chaine,avant,apres):
    """ Remplace les occurrences de avant par apres dans chaine."""

def Extraire(chaine,separation,n):
    """ Retourne la valeur du nième champ de chaine.
        Les champs sont séparés par le caractÃ¨re séparation."""

def Format(n):
    """ Retourne une chaine de caractères de 4 caractères pour tout nombre entier de 0 à  9999
        Les valeurs seront prÃ©cÃ©dÃ©es de 0."""

def main():
    """ Tests, toutes les lignes sont correctes (résultat : True). Complèter les fonctions."""
    print (Xor(" $--.","A")=="Hello")
    print (Xor("-/+-43","ABA")=="Bonjour")
    print (Indice([1,2,3,4,5,6],3)==2)
    print (EncodeBase64(b"Une Chaine")==b"VW5lIENoYWluZQ==")
    print (DecodeBase64(b"VW5lIENoYWluZQ==")==b"Une Chaine")
    print (EncodeAES(b"Elements",[2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,6])==b"\xcb[q(\x07\xe70\xff\x13D\xfe\xf9\x9eQ\x08!")
    print (DecodeAES(b"\xcb[q(\x07\xe70\xff\x13D\xfe\xf9\x9eQ\x08!",[2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,6])==b"Elements")
    print (Contient("OK","Le resultat est OK !")==True)
    print (Contient("OK","Le resultat est Ok !")==False)
    print (EstImprimable("A")==True)
    print (EstImprimable("\x07")==False)
    print (EstImprimable(" ")==True)
    print (Remplace("Ceci est une string typique","string","chaine")=="Ceci est une chaine typique")
    print (Extraire("ROUGE,0034,4EF563",",",1)==34)
    print (Format(3)=="0003")
    print (Format(123)=="0123")
    return


if __name__ == '__main__':
    main()
